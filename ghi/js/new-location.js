console.log("hello")

window.addEventListener('DOMContentLoaded', async()=> {
    const url = 'http://localhost:8000/api/states/';
    try {
        const response = await fetch(url);
        if (!response.ok) {
            throw new Error('Response not ok');
        } else {
            const data = await response.json();
            const selectTag = document.getElementById('state');
            for (let state of data.states) {
                const option = document.createElement('option');
                option.value = state.abbreviation;
                option.innerHTML =state.name;
                selectTag.appendChild(option);
            }
            //FormData object expects name attributes on the form inputs. Name="" should equal the same as the ID attributes or what the json object should have as a key. \\
            const formTag = document.getElementById('create-location-form');
            formTag.addEventListener('submit', async event => {
                event.preventDefault();
                const formData = new FormData(formTag);
                const json = JSON.stringify(Object.fromEntries(formData));
                console.log("json:", json)

                const fetchConfig = {
                  method: 'post',
                  body: json,
                  headers: {
                    'Content-Type': 'application/json',
                  },
                };

                const newLocationUrl = 'http://localhost:8000/api/locations/';
                const newLocationResponse = await fetch(newLocationUrl, fetchConfig);
                if (newLocationResponse.ok) {
                  formTag.reset();
                  const newLocation = await newLocationResponse.json();
                  console.log(newLocation);
                }
                })

    }}
    catch (error) {
        console.error('error', error); }
})

// const optionHTML = addState(state);
// selectTag.insertAdjacentHTML('beforeend', optionHTML);
