window.addEventListener('DOMContentLoaded', async () => {
    const selectTag = document.getElementById('conference');
    const selectLoadingTag = document.getElementById('loading-conference-spinner');
    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    try{
        if (!response.ok) {
            throw new Error('Response not ok');
        } else {
            const data = await response.json();

            for (let conference of data.conferences) {
                const option = document.createElement('option');
                option.value = conference.href;
                option.innerHTML = conference.name;
                selectTag.appendChild(option);
            }
            selectLoadingTag.classList.add('d-none')
            selectTag.classList.remove('d-none')

            const formTag = document.getElementById('create-attendee-form');
            formTag.addEventListener('submit', async event =>{
                event.preventDefault();
                const formData = new FormData(formTag);
                const json = JSON.stringify(Object.fromEntries(formData));
                console.log("json:", json)

                const fetchConfig = {
                    method: 'post',
                    body: json,
                    headers: {
                        'Content-Type': 'application/json'
                    }
                };
                const newAttendeeUrl = `http://localhost:8001/api/attendees/`
                const newAttendeeResponse = await fetch(newAttendeeUrl, fetchConfig);
                const selectSuccessTag = document.getElementById('success-message')
                if (newAttendeeResponse.ok) {
                    formTag.reset();
                    const newAttendee = await newAttendeeResponse.json();
                    console.log(newAttendee)

                    formTag.classList.add('d-none')
                    selectSuccessTag.classList.remove('d-none')
                    }
                })
            }
    } catch (error) {
        console.error('error', error)
    }
})
