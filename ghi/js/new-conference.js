window.addEventListener("DOMContentLoaded", async()=> {
    const url = 'http://localhost:8000/api/locations/';

    try {
        const response = await fetch(url);
        if (!response.ok) {
            throw new Error('Response not ok')
        } else {
            const data = await response.json();
            const selectTag = document.getElementById('location');

        for (let location of data.locations) {
            const option = document.createElement('option');
            option.value = location.id;
            option.innerHTML = location.name;
            selectTag.appendChild(option)
        }
        const formTag = document.getElementById('create-conference-form');
        formTag.addEventListener('submit', async event =>{
            event.preventDefault();
            const formData = new FormData(formTag);
            const json = JSON.stringify(Object.fromEntries(formData));
            console.log(json)

            const fetchConfig = {
                method: 'post',
                body: json,
                headers: {
                    'Content-Type': 'application/json'
                }
            }
            const newConferenceUrl = 'http://localhost:8000/api/conferences/';
            const newConferenceResponse = await fetch(newConferenceUrl, fetchConfig)
            if (newConferenceResponse.ok) {
                formTag.reset();
                const newConference = await newConferenceResponse.json();
                console.log(newConference)
            }
        })
    }
        }
        catch (error) {
            console.error('error', error); }
    })
