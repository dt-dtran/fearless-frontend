window.addEventListener('DOMContentLoaded', async () => {


    // this is the conference api request and select input section
    const url = 'http://localhost:8000/api/conferences/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();

      const selectTag = document.getElementById('conference');
      for (let conference of data.conferences) {
        // Create an 'option' element
        const option = document.createElement('option');
        // Set the '.value' property of the option element to the
        // conference id
        option.value = conference.id;
        // Set the '.innerHTML' property of the option element to
        // the conference name
        option.innerHTML = conference.name;
        // Append the option element as a child of the select tag
        selectTag.appendChild(option);
      }
    }

    // this is the form stuff
    const formTag = document.getElementById('create-presentation-form');
    formTag.addEventListener('submit', async event => {
        event.preventDefault();

    const formData = new FormData(formTag);
    const obj = Object.fromEntries(formData);
    const conferenceID = obj.conference;
    const json = JSON.stringify(Object.fromEntries(formData));

    // const parsed = JSON.parse(json)
    // const conferenceID = parsed['conference']

    const presentationURL = `http://localhost:8000/api/conferences/${conferenceID}/presentations/`;
    const fetchConfig = {
        method: "post",
        body: json,
        headers: {
            'Content-Type': 'application/json',
        },
    };
    const formResponse = await fetch(presentationURL, fetchConfig);
    if (formResponse.ok) {
        formTag.reset();
        const newPresentation = await formResponse.json();
        console.log(newPresentation);
    }
    })
});
