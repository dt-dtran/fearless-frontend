import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
// import {
//   createBrowserRouter,
//   RouterProvider,
// } from 'react-router-dom';

// "The line above imports the App function from the App.js file
// 'root' and 'root.render' is telling React where to take that fake HTML and put it."
//  <App /> is what runs the App function in the other file."
// router stuff
//const router = createBrowserRouter([
//   {
//     path: "/",
//     element: <Root />,
//   },
// ]);

const root = ReactDOM.createRoot(document.getElementById('root'));
// root.render(
//   <React.StrictMode>
//     <App attendees={data.attendees}/>
//   </React.StrictMode>
// );

// const response = await fetch('http://localhost:8001/api/attendees/'); will not work because React is using a tool called Babel
// Babel to translate the JSX files into JavaScript. So, your JavaScript is not yours, it's Babel's." you have use async funcition {} wrapped

async function loadAttendees() {
  const response = await fetch('http://localhost:8001/api/attendees/');
  console.log(response);
  if (response.ok) {
    const data = await response.json()
    root.render(
      <React.StrictMode>
        {/* <RouterProvider router={router} /> */}
        <App attendees={data.attendees} />
      </React.StrictMode>
    );
  } else {
    console.error(response)
  }
}

loadAttendees();
// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
