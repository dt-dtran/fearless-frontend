import './App.css';
import Nav from './Nav';
import AttendeesList from './AttendeesList'
import LocationForm from './LocationForm'
import ConferenceForm from './ConferenceForm'
import AttendeeConferenceForm from './AttendeeConferenceForm'
import PresentationForm from './PresentationForm'
import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from './MainPage';


//the attendees attribute on the App in the index.js, <App attendees={data.attendees}/> sets a variable named 'attendees' to the value inside the curly braces. Then, that variable becomes a property on the props parameter that gets passed into the function. "That's how you end up passing arguments to the React functions. They call them components, by the way, the functions in React that return JSX."

function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
        <Route index element={<MainPage />} />
          <Route path="locations/new" element={<LocationForm />}> </Route>
          <Route path="conferences/new" element={<ConferenceForm />}></Route>
          <Route path="attendees/new" element={<AttendeeConferenceForm />}></Route>
          <Route path="presentations/new" element={<PresentationForm />}></Route>
          <Route path="attendees" element={<AttendeesList attendees={props.attendees} />}></Route>
      </Routes>
    </div>
    </BrowserRouter>
  );
}

export default App;

{/* <Nav />
    <LocationForm />
    <ConferenceForm />
    <AttendeeConferenceForm />
    <PresentationForm />
    <AttendeesList attendees={props.attendees} /> */}


    // <Route path="locations/new" element={<LocationForm />} />
    // <Route path="conferences/new" element={<ConferenceForm />} />
    // <Route path="attendees/new" element={<AttendeeConferenceForm />} />
    // <Route path="presentations/new" element={<PresentationForm />} />
    // <Route path="attendees" element={<AttendeesList attendees={props.attendees} />} />


    // <Routes>
    // <Route index element={<MainPage />} />

    //     <Route path="locations">
    //       <Route path="new" element={<LocationForm />} />
    //     </Route>
    //     <Route path="conferences">
    //       <Route path="new" element={<ConferenceForm />} />
    //     </Route>
    //     <Route path="attendees">
    //       <Route path="list" element={<AttendeesList attendees={props.attendees}/>} />
    //     </Route>
    //     <Route path="attendees">
    //       <Route path="new" element={<AttendeeConferenceForm />} />
    //     </Route>
    //     <Route path="presentations">
    //       <Route path="new" element={<PresentationForm />} />
    //     </Route>
    //   </Routes>
